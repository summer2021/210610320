# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.16.3)

# We name the project and the target for the ExternalProject_Add() call
# to something that will highlight to the user what we are working on if
# something goes wrong and an error message is produced.

project(jpeg_turbo-populate NONE)

include(ExternalProject)
ExternalProject_Add(jpeg_turbo-populate
                     "UPDATE_DISCONNECTED" "False" "URL" "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/2.0.4.tar.gz" "URL_HASH" "MD5=44c43e4a9fb352f47090804529317c88"
                    SOURCE_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/jpeg_turbo-src"
                    BINARY_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/jpeg_turbo-build"
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    USES_TERMINAL_DOWNLOAD  YES
                    USES_TERMINAL_UPDATE    YES
)
