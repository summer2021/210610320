# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.16.3)

# We name the project and the target for the ExternalProject_Add() call
# to something that will highlight to the user what we are working on if
# something goes wrong and an error message is produced.

project(projectq-populate NONE)

include(ExternalProject)
ExternalProject_Add(projectq-populate
                     "UPDATE_DISCONNECTED" "False" "URL" "https://github.com/ProjectQ-Framework/ProjectQ/archive/v0.5.1.tar.gz " "URL_HASH" "MD5=13430199c253284df8b3d840f11d3560"
                    SOURCE_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/projectq-src"
                    BINARY_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/projectq-build"
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    USES_TERMINAL_DOWNLOAD  YES
                    USES_TERMINAL_UPDATE    YES
)
