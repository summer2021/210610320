# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.16.3)

# We name the project and the target for the ExternalProject_Add() call
# to something that will highlight to the user what we are working on if
# something goes wrong and an error message is produced.

project(ompi-populate NONE)

include(ExternalProject)
ExternalProject_Add(ompi-populate
                     "UPDATE_DISCONNECTED" "False" "URL" "https://github.com/open-mpi/ompi/archive/v4.0.3.tar.gz" "URL_HASH" "MD5=86cb724e8fe71741ad3be4e7927928a2"
                    SOURCE_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/ompi-src"
                    BINARY_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/ompi-build"
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    USES_TERMINAL_DOWNLOAD  YES
                    USES_TERMINAL_UPDATE    YES
)
