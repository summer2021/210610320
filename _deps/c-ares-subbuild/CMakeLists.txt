# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.16.3)

# We name the project and the target for the ExternalProject_Add() call
# to something that will highlight to the user what we are working on if
# something goes wrong and an error message is produced.

project(c-ares-populate NONE)

include(ExternalProject)
ExternalProject_Add(c-ares-populate
                     "UPDATE_DISCONNECTED" "False" "URL" "https://github.com/c-ares/c-ares/releases/download/cares-1_15_0/c-ares-1.15.0.tar.gz" "URL_HASH" "MD5=d2391da274653f7643270623e822dff7"
                    SOURCE_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/c-ares-src"
                    BINARY_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/c-ares-build"
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    USES_TERMINAL_DOWNLOAD  YES
                    USES_TERMINAL_UPDATE    YES
)
