INDEX_NAME = res_index
IN_DIR = $(srcdir)
SRC_DIR = $(srcdir)

DIRS = $(TMP_DIR)/dirs.timestamp

$(TMP_DIR)/dirs.timestamp:
	$(MKINSTALLDIRS) $(OUT_DIR) $(TMP_DIR)
	echo timestamp > $(TMP_DIR)/dirs.timestamp

$(OUT_DIR)/nfkc.nrm: $(IN_DIR)/in/nfkc.nrm  | $(DIRS)
	$(INVOKE) $(TOOLBINDIR)/icupkg -t$(ICUDATA_CHAR) $(IN_DIR)/in/nfkc.nrm $(OUT_DIR)/nfkc.nrm

$(OUT_DIR)/nfkc_cf.nrm: $(IN_DIR)/in/nfkc_cf.nrm  | $(DIRS)
	$(INVOKE) $(TOOLBINDIR)/icupkg -t$(ICUDATA_CHAR) $(IN_DIR)/in/nfkc_cf.nrm $(OUT_DIR)/nfkc_cf.nrm

$(OUT_DIR)/uts46.nrm: $(IN_DIR)/in/uts46.nrm  | $(DIRS)
	$(INVOKE) $(TOOLBINDIR)/icupkg -t$(ICUDATA_CHAR) $(IN_DIR)/in/uts46.nrm $(OUT_DIR)/uts46.nrm

define ICUDATA_LIST_CONTENT
nfkc.nrm
nfkc_cf.nrm
uts46.nrm
endef
export ICUDATA_LIST_CONTENT

$(TMP_DIR)/icudata.lst:   | $(DIRS)
	echo "$$ICUDATA_LIST_CONTENT" > $(TMP_DIR)/icudata.lst

ICUDATA_ALL_OUTPUT_FILES = $(OUT_DIR)/nfkc.nrm \
		$(OUT_DIR)/nfkc_cf.nrm \
		$(OUT_DIR)/uts46.nrm \
		$(TMP_DIR)/icudata.lst


