
#define OPENCV_INSTALL_PREFIX "/home/zzb/programs/mindspore/mindspore/.mslib/opencv_0c806d63181617f23e373893ff6e3283"

#define OPENCV_DATA_INSTALL_PATH "share/opencv4"

#define OPENCV_BUILD_DIR "/home/zzb/programs/mindspore/mindspore/_deps/opencv-src/_build"

#define OPENCV_DATA_BUILD_DIR_SEARCH_PATHS \
    "..//"

#define OPENCV_INSTALL_DATA_DIR_RELATIVE "../share/opencv4"
