# CMake generated Testfile for 
# Source directory: /home/zzb/programs/mindspore/mindspore/_deps/opencv-src
# Build directory: /home/zzb/programs/mindspore/mindspore/_deps/opencv-src/_build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("3rdparty/zlib")
subdirs("3rdparty/libjpeg-turbo")
subdirs("3rdparty/libpng")
subdirs("3rdparty/openexr")
subdirs("3rdparty/quirc")
subdirs("3rdparty/ittnotify")
subdirs("include")
subdirs("modules")
subdirs("doc")
subdirs("data")
