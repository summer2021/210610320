# CMake generated Testfile for 
# Source directory: /home/zzb/programs/mindspore/mindspore/_deps/tiff-src/contrib
# Build directory: /home/zzb/programs/mindspore/mindspore/_deps/tiff-src/_build/contrib
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("addtiffo")
subdirs("dbs")
subdirs("iptcutil")
subdirs("mfs")
subdirs("pds")
subdirs("ras")
subdirs("stream")
subdirs("tags")
subdirs("win_dib")
