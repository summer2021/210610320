# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.16.3)

# We name the project and the target for the ExternalProject_Add() call
# to something that will highlight to the user what we are working on if
# something goes wrong and an error message is produced.

project(tiff-populate NONE)

include(ExternalProject)
ExternalProject_Add(tiff-populate
                     "UPDATE_DISCONNECTED" "False" "URL" "http://download.osgeo.org/libtiff/tiff-4.2.0.tar.gz" "URL_HASH" "MD5=2bbf6db1ddc4a59c89d6986b368fc063"
                    SOURCE_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/tiff-src"
                    BINARY_DIR          "/home/zzb/programs/mindspore/mindspore/_deps/tiff-build"
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    USES_TERMINAL_DOWNLOAD  YES
                    USES_TERMINAL_UPDATE    YES
)
