# CMake generated Testfile for 
# Source directory: /home/zzb/programs/mindspore/mindspore/_deps/onednn-src
# Build directory: /home/zzb/programs/mindspore/mindspore/_deps/onednn-src/_build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("examples")
subdirs("tests")
